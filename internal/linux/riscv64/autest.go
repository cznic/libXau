// Code generated for linux/riscv64 by 'gcc --prefix-enumerator=_ --prefix-external=x_ --prefix-field=F --prefix-macro=m_ --prefix-static-internal=_ --prefix-static-none=_ --prefix-tagged-enum=_ --prefix-tagged-struct=T --prefix-tagged-union=T --prefix-typename=T --prefix-undefined=_ -ignore-link-errors -extended-errors -DNDEBUG -o Autest.go Autest.o.go ./.libs/libXau.a', DO NOT EDIT.

//go:build linux && riscv64

package main

import (
	"reflect"
	"unsafe"

	"modernc.org/libc"
)

var _ reflect.Type
var _ unsafe.Pointer

type TXauth = struct {
	Ffamily         uint16
	Faddress_length uint16
	Faddress        uintptr
	Fnumber_length  uint16
	Fnumber         uintptr
	Fname_length    uint16
	Fname           uintptr
	Fdata_length    uint16
	Fdata           uintptr
}

func x_main(tls *libc.TLS, argc int32, argv uintptr) (r int32) {
	bp := tls.Alloc(64)
	defer tls.Free(64)
	var data, file, name, output, v1, v2 uintptr
	var state, v3, v4 int32
	var _ /* test_data at bp+0 */ TXauth
	_, _, _, _, _, _, _, _, _ = data, file, name, output, state, v1, v2, v3, v4
	name = __ccgo_ts
	data = __ccgo_ts + 11
	file = libc.UintptrFromInt32(0)
	state = 0
	for {
		argv += 8
		v1 = argv
		if !(*(*uintptr)(unsafe.Pointer(v1)) != 0) {
			break
		}
		if !(libc.Xstrcmp(tls, *(*uintptr)(unsafe.Pointer(argv)), __ccgo_ts+60) != 0) {
			argv += 8
			v2 = argv
			file = *(*uintptr)(unsafe.Pointer(v2))
		} else {
			if state == 0 {
				name = *(*uintptr)(unsafe.Pointer(argv))
				state++
			} else {
				if state == int32(1) {
					data = *(*uintptr)(unsafe.Pointer(argv))
					state++
				}
			}
		}
	}
	(*(*TXauth)(unsafe.Pointer(bp))).Ffamily = uint16(0)
	(*(*TXauth)(unsafe.Pointer(bp))).Faddress_length = uint16(0)
	(*(*TXauth)(unsafe.Pointer(bp))).Faddress = __ccgo_ts + 66
	(*(*TXauth)(unsafe.Pointer(bp))).Fnumber_length = uint16(0)
	(*(*TXauth)(unsafe.Pointer(bp))).Fnumber = __ccgo_ts + 66
	(*(*TXauth)(unsafe.Pointer(bp))).Fname_length = uint16(libc.Xstrlen(tls, name))
	(*(*TXauth)(unsafe.Pointer(bp))).Fname = name
	(*(*TXauth)(unsafe.Pointer(bp))).Fdata_length = uint16(libc.Xstrlen(tls, data))
	(*(*TXauth)(unsafe.Pointer(bp))).Fdata = data
	if !(file != 0) {
		output = libc.Xtmpfile(tls)
	} else {
		output = libc.Xfopen(tls, file, __ccgo_ts+67)
	}
	if output != 0 {
		state = x_XauWriteAuth(tls, output, bp)
		libc.Xfclose(tls, output)
	}
	v4 = libc.Int32FromInt32(1)
	state = v4
	if v4 != 0 {
		v3 = 0
	} else {
		v3 = int32(1)
	}
	return v3
}

func main() {
	libc.Start(x_main)
}

/* Return values from XauLockAuth */

func _write_short(tls *libc.TLS, s uint16, file uintptr) (r int32) {
	bp := tls.Alloc(16)
	defer tls.Free(16)
	var _ /* file_short at bp+0 */ [2]uint8
	(*(*[2]uint8)(unsafe.Pointer(bp)))[0] = uint8(uint32(s) & libc.Uint32FromInt32(0xff00) >> int32(8))
	(*(*[2]uint8)(unsafe.Pointer(bp)))[int32(1)] = libc.Uint8FromInt32(libc.Int32FromUint16(s) & int32(0xff))
	if libc.Xfwrite(tls, bp, uint64(2), uint64(1), file) != uint64(1) {
		return 0
	}
	return int32(1)
}

func _write_counted_string(tls *libc.TLS, count uint16, string1 uintptr, file uintptr) (r int32) {
	if _write_short(tls, count, file) == 0 {
		return 0
	}
	if libc.Xfwrite(tls, string1, uint64(1), uint64(count), file) != uint64(count) {
		return 0
	}
	return int32(1)
}

func x_XauWriteAuth(tls *libc.TLS, auth_file uintptr, auth uintptr) (r int32) {
	if _write_short(tls, (*TXauth)(unsafe.Pointer(auth)).Ffamily, auth_file) == 0 {
		return 0
	}
	if _write_counted_string(tls, (*TXauth)(unsafe.Pointer(auth)).Faddress_length, (*TXauth)(unsafe.Pointer(auth)).Faddress, auth_file) == 0 {
		return 0
	}
	if _write_counted_string(tls, (*TXauth)(unsafe.Pointer(auth)).Fnumber_length, (*TXauth)(unsafe.Pointer(auth)).Fnumber, auth_file) == 0 {
		return 0
	}
	if _write_counted_string(tls, (*TXauth)(unsafe.Pointer(auth)).Fname_length, (*TXauth)(unsafe.Pointer(auth)).Fname, auth_file) == 0 {
		return 0
	}
	if _write_counted_string(tls, (*TXauth)(unsafe.Pointer(auth)).Fdata_length, (*TXauth)(unsafe.Pointer(auth)).Fdata, auth_file) == 0 {
		return 0
	}
	return int32(1)
}

var __ccgo_ts = (*reflect.StringHeader)(unsafe.Pointer(&__ccgo_ts1)).Data

var __ccgo_ts1 = "XAU-TEST-1\x00Do not begin the test until instructed to do so.\x00-file\x00\x00w\x00/.Xauthority\x00XAUTHORITY\x00HOME\x00%s%s\x00rb\x00%s-c\x00%s-l\x00"
